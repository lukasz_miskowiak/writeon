export default {
    title: 'Section',
    name: 'section',
    type:  'document',
    fields: [
        {
            title: 'Name',
            name: 'name',
            type: 'string',
        },
        {
            title: 'Content',
            name: 'content',
            type: 'array',
            of: [
                {
                    type: 'block',
                },
                {
                    type: 'audio',
                },
                {
                    type: 'video',
                },
                {
                    type: 'exercise',
                }
            ]
        }
    ]
}
