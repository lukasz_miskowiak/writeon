export default {
    title: 'Glossary',
    name: 'glossary',
    type: 'document',
    fields: [
        {
            title: 'Name',
            name: 'name',
            type: 'string',
        },
        {
            title: 'Glossary',
            name: 'glossary',
            type: 'array',
            of: [
                {
                    type: 'object',
                    fields: [
                        {
                            title: 'Term',
                            name: 'term',
                            type: 'string',
                        },
                        {
                            title: 'Definition',
                            name: 'definition',
                            type: 'string'
                        }
                    ]
                }
            ]
        }
    ]
}