export default {
    title: 'Video',
    name: 'video',
    type: 'object',
    fields: [
        {
            title: 'Name',
            name: 'name',
            type: 'string',
        },
        {
            title: 'File',
            name: 'file',
            type: 'file',
        },
        {
            title: 'Transcryption',
            name: 'transcryption',
            type: 'array',
            of: [
                {
                    type: 'block',
                }
            ]
        }
    ]
};
