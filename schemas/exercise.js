export default {
    title: 'Exercise',
    name: 'exercise',
    type: 'object',
    fields: [
        {
            title: 'Name',
            name: 'name',
            type: 'string',
        },
        {
            title: 'Url',
            name: 'url',
            type: 'url',
        }
    ]
};
