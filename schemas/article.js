export default {
    title: 'Article',
    name: 'article',
    type: 'document',
    fields: [
        {
            title: 'Name',
            name: 'name',
            type: 'string'
        },
        {
            title: 'Sections',
            name: 'sections',
            type: 'array',
            of: [
                {
                    type: 'reference',
                    to: [
                        {
                            type: 'section',
                        }
                    ],
                },
            ]
        },
        {
            title: 'Glossary',
            name: 'glossary',
            type: 'reference',
            to: [
                {
                    type: 'glossary'
                }
            ]
        }
    ]
}