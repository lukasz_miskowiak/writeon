export default {
    title: 'Map',
    name: 'map',
    type: 'document',
    fields: [
        {
            title: 'Name',
            name: 'name',
            type: 'string'
        },
        {
            title: 'Tile',
            name: 'tile',
            type: 'array',
            of: [
                {
                    type: 'object',
                    fields: [
                        {
                            title: 'Name',
                            name: 'name',
                            type: 'string'
                        },
                        {
                            title: 'Image',
                            name: 'image',
                            type: 'image',
                        },
                        {
                            title: 'Links',
                            name: 'links',
                            type: 'array',
                            of: [
                                {
                                    type: 'reference',
                                    to: [
                                        {
                                            type: 'article',
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
}