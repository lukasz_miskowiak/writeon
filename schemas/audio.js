export default {
    title: 'Audio',
    name: 'audio',
    type: 'object',
    fields: [
        {
            title: 'Name',
            name: 'name',
            type: 'string',
        },
        {
            title: 'File',
            name: 'file',
            type: 'file',
        },
    ]
};
