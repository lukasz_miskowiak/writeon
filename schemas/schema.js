const createSchema = require('part:@sanity/base/schema-creator')
const schemaTypes = require('all:part:@sanity/base/schema-type')
import exercise from './exercise';
import video from './video';
import audio from './audio';
import section from './section';
import glossary from './glossary';
import article from './article';
import map from './map';


module.exports = createSchema({
  name: 'default',
  types: schemaTypes.concat([ map, article, glossary, section, audio, video, exercise ])
})
